package com.sp.mvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrintTest {

	private static final Logger logger = LoggerFactory.getLogger(PrintTest.class);
	
	public static void main(String[] args) {
		logger.debug("로그 찍음");
	}
}
