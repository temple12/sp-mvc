package com.sp.mvc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sp.mvc.service.CommentBoardService;
import com.sp.mvc.vo.CommentBoardVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:src\\main\\webapp\\WEB-INF\\spring\\root-context.xml")
public class DAOTest {
	
	@Autowired
	private CommentBoardService cbService;
	private CommentBoardVO cbVO;
	
	
	@Before
	public void before() {
		cbVO = new CommentBoardVO();
		cbVO.setCmTitle("서비스 제목 테스트 수정");
		cbVO.setCmContent("서비스 내용 테스트 수정");
		cbVO.setMiNum(1);
		cbVO.setCmNum(6);
	}
	@Test
	public void test() {
		List<CommentBoardVO> cb = cbService.selectCommentBoardList(cbVO);
		assertThat(cb, is(nullValue()));
	}

}
