package com.sp.mvc.controller.common;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.sp.mvc.controller.HomeController;

@Controller
public class URIController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@GetMapping("/views/**")
	public String goPage(HttpServletRequest req) {
		String uri = req.getRequestURI();
		logger.info("uri=>{}", uri);
		return uri.substring(7);
	}
}
