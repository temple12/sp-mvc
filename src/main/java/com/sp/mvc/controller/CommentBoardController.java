package com.sp.mvc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sp.mvc.service.CommentBoardService;
import com.sp.mvc.vo.CommentBoardVO;

@Controller
public class CommentBoardController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	private CommentBoardService cbService;
	
	@GetMapping("/cms")
	public @ResponseBody List<CommentBoardVO> commentBoardList(CommentBoardVO cbVO){
		return cbService.selectCommentBoardList(cbVO);
	}
	@GetMapping("/cm")
	public @ResponseBody CommentBoardVO commentBoard(CommentBoardVO cbVO){
		return cbService.selectCommentBoard(cbVO);
	}
	
	@PostMapping("/cmu")
	public @ResponseBody int commentBoardUpdate(CommentBoardVO cbVO){
		return cbService.updateCommentBoard(cbVO);
	}
	
	@PostMapping("/cmd")
	public @ResponseBody int commentBoardDelete(CommentBoardVO cbVO){
		return cbService.deleteCommentBoard(cbVO);
	}
	
	@PostMapping("/cmi")
	public @ResponseBody int commentBoardInsert(CommentBoardVO cbVO){
		return cbService.insertCommentBoard(cbVO);
	}
	
}
