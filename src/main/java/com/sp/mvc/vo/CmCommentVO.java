package com.sp.mvc.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("cc")
public class CmCommentVO {
	private int ccNum;
	private int cmNum;
	private String ccContent;
	private int miNum;
	private String credat;
	private String cretim;
	private String moddat;
	private String modtim;
}
