package com.sp.mvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.mvc.dao.HomeDAO;
import com.sp.mvc.service.HomeService;

@Service
public class HomeServiceImpl implements HomeService {
	@Autowired
	private HomeDAO homeDAO;
	
	@Override
	public int test() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<String> getList() {
		return homeDAO.getList();
	}

}
