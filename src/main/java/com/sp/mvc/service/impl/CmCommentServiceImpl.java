package com.sp.mvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.mvc.dao.CmCommentDAO;
import com.sp.mvc.service.CmCommentService;
import com.sp.mvc.vo.CmCommentVO;

@Service
public class CmCommentServiceImpl implements CmCommentService {
	@Autowired
	private CmCommentDAO cmDAO;
	
	@Override
	public int insertCmComment(CmCommentVO cmVO) {
		return cmDAO.insertCmComment(cmVO);
	}

	@Override
	public List<CmCommentVO> selectCmCommentList(CmCommentVO cmVO) {
		return cmDAO.selectCmCommentList(cmVO);
	}

	@Override
	public int deleteCmComment(CmCommentVO cmVO) {
		return cmDAO.deleteCmComment(cmVO);
	}

	@Override
	public int updateCmComment(CmCommentVO cmVO) {
		return cmDAO.updateCmComment(cmVO);
	}

}
