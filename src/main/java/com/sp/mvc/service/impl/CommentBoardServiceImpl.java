package com.sp.mvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.mvc.dao.CommentBoardDAO;
import com.sp.mvc.service.CommentBoardService;
import com.sp.mvc.vo.CommentBoardVO;

@Service
public class CommentBoardServiceImpl implements CommentBoardService {
	@Autowired
	private CommentBoardDAO cbDAO;
	
	@Override
	public int insertCommentBoard(CommentBoardVO cbVO) {
		return cbDAO.insertCommentBoard(cbVO);
	}

	@Override
	public List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cbVO) {
		return cbDAO.selectCommentBoardList(cbVO);
	}

	@Override
	public CommentBoardVO selectCommentBoard(CommentBoardVO cbVO) {
		return cbDAO.selectCommentBoard(cbVO);
	}

	@Override
	public int deleteCommentBoard(CommentBoardVO cbVO) {
		return cbDAO.deleteCommentBoard(cbVO);
	}

	@Override
	public int updateCommentBoard(CommentBoardVO cbVO) {
		return cbDAO.updateCommentBoard(cbVO);
	}

}
