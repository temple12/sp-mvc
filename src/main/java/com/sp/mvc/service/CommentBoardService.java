package com.sp.mvc.service;

import java.util.List;

import com.sp.mvc.vo.CommentBoardVO;

public interface CommentBoardService {
	int insertCommentBoard(CommentBoardVO cbVO);
	List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cbVO);
	CommentBoardVO selectCommentBoard(CommentBoardVO cbVO);
	int deleteCommentBoard(CommentBoardVO cbVO);
	int updateCommentBoard(CommentBoardVO cbVO);
}
