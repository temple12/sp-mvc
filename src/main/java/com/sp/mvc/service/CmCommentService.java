package com.sp.mvc.service;

import java.util.List;

import com.sp.mvc.vo.CmCommentVO;

public interface CmCommentService {
	int insertCmComment(CmCommentVO cmVO);
	List<CmCommentVO> selectCmCommentList(CmCommentVO cmVO);
	int deleteCmComment(CmCommentVO cmVO);
	int updateCmComment(CmCommentVO cmVO);
}
