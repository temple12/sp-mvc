package com.sp.mvc.dao;

import java.util.List;

import com.sp.mvc.vo.CmCommentVO;

public interface CmCommentDAO {
	int insertCmComment(CmCommentVO cmVO);
	List<CmCommentVO> selectCmCommentList(CmCommentVO cmVO);
	int deleteCmComment(CmCommentVO cmVO);
	int updateCmComment(CmCommentVO cmVO);
}
	