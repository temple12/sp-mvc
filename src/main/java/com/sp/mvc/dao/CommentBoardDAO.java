package com.sp.mvc.dao;

import java.util.List;

import com.sp.mvc.vo.CommentBoardVO;

public interface CommentBoardDAO {
	int insertCommentBoard(CommentBoardVO cbVO);
	List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cbVO);
	CommentBoardVO selectCommentBoard(CommentBoardVO cbVO);
	int deleteCommentBoard(CommentBoardVO cbVO);
	int updateCommentBoard(CommentBoardVO cbVO);
}
