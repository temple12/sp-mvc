package com.sp.mvc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sp.mvc.dao.HomeDAO;

@Repository
public class HomeDAOImpl implements HomeDAO {

	@Override
	public int test() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<String> getList() {
		List<String> list = new ArrayList<String>();
		list.add("테스트");
		list.add("123");
		return list;
	}

}
