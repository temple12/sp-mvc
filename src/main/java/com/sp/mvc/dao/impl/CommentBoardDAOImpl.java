package com.sp.mvc.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sp.mvc.dao.CommentBoardDAO;
import com.sp.mvc.vo.CommentBoardVO;

@Repository
public class CommentBoardDAOImpl implements CommentBoardDAO {
	@Autowired
	private SqlSession ss;
	@Override
	public int insertCommentBoard(CommentBoardVO cbVO) {
			return ss.insert("CommentBoard.insertCommentBoard", cbVO);
	}
	@Override
	public List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cbVO) {
			return ss.selectList("CommentBoard.selectCommentBoardList", cbVO);
	}
	@Override
	public CommentBoardVO selectCommentBoard(CommentBoardVO cbVO) {
			return ss.selectOne("CommentBoard.selectCommentBoard", cbVO);
	}
	@Override
	public int deleteCommentBoard(CommentBoardVO cbVO) {
			return ss.delete("CommentBoard.deleteCommentBoard", cbVO);
	}
	@Override
	public int updateCommentBoard(CommentBoardVO cbVO) {
			return ss.update("CommentBoard.updateCommentBoard", cbVO);
	}
}
