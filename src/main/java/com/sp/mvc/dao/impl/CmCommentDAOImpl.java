package com.sp.mvc.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sp.mvc.dao.CmCommentDAO;
import com.sp.mvc.vo.CmCommentVO;

@Repository
public class CmCommentDAOImpl implements CmCommentDAO {
	@Autowired
	private SqlSessionFactory ssf;
	
	@Override
	public int insertCmComment(CmCommentVO cmVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.insert("CmComment.insertCmContent", cmVO);
		}
	}

	@Override
	public List<CmCommentVO> selectCmCommentList(CmCommentVO cmVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.selectList("CmComment.selectCmCommentList", cmVO);
		}
	}

	@Override
	public int deleteCmComment(CmCommentVO cmVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.delete("CmComment.deleteCmComment", cmVO);
		}
	}

	@Override
	public int updateCmComment(CmCommentVO cmVO) {
		try(SqlSession ss = ssf.openSession()){
			return ss.update("CmComment.updateCmComment", cmVO);
		}
	}

}
