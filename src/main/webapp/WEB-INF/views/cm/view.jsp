<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
</head>
<body>
<table id="tb" border="1">

</table><br>
<script>
$(document).ready(function(){
var conf = {
		url : '/cm',
		method : 'GET',
		data : {
			cmNum : ${param.cmNum}
		},
		success : function(res){
			var html ='';
			html+= '<tr><th>번호</th><td>'+res.cmNum+'</td></tr>';
			html+= '<tr><th>제목</th><td><input type="text" id="cmTitle" value='+res.cmTitle+'></td></tr>';
			html+= '<tr><th>내용</th><td><textarea id="cmContent">'+res.cmContent+'</textarea></td></tr>';
			html+= '<tr><th>작성자</th><td>'+res.miNum+'</td></tr>';
			html+= '<tr><th>생성일</th><td>'+res.credat+'</td></tr>';
			html+= '<tr><th>생성시간</th><td>'+res.cretim+'</td></tr>';
			html+= '<tr><th>수정일</th><td>'+res.moddat+'</td></tr>';
			html+= '<tr><th>수정시간</th><td>'+res.modtim+'</td></tr>';
			document.querySelector('#tb').innerHTML = html;
		},error : function(error){
			
		}
	}
	$.ajax(conf);
});
function doUpdate(){
	var conf = {
			url : '/cmu',
			method : 'POST',
			data : {
				cmNum : ${param.cmNum},
				cmTitle : $('#cmTitle').val(),
				cmContent : $('#cmContent').val()
			},
			success : function(res){
				if(res === 1){
					alert('수정 성공');
					location.href = '/views/cm/list';
				}else{
					aelrt('수정 실패');
				}
			},error : function(error){
				
			}
		}
		$.ajax(conf);
}
function doDelete(){
	var conf = {
			url : '/cmd',
			method : 'POST',
			data : {
				cmNum : ${param.cmNum}
			},
			success : function(res){
				if(res === 1){
					alert('삭제 성공');
					location.href = '/views/cm/list';
				}else{
					aelrt('삭제 실패');
				}
			},error : function(error){
				
			}
		}
		$.ajax(conf);
}
</script>
<button type="button" onclick="doUpdate()">수정</button>
<button type="button" onclick="doDelete()">삭제</button>
<a href="/views/cm/list"><button type="button">리스트</button></a>
</body>
</html>